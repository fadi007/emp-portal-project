# Employee Portal Project

## Project Description

This small python web-project is designed in python FlasK and is deployed on Heroku platform. The reason for choosing Heroku is that we have packaged the project in docker and shipped the image as a containerized application with all the dependencies included which are mentioned in requirements.txt file. Heroku is a PaaS based platform which offers a ready-container environment for deployment of containerized application. 

For database, I have used FLASK-SQLAlchemy and for running on Heroku, I have used gunicorn as multiple python processes need to run in a single dyno. 

## Installation

All the python dependencies are packaged together. No need to install anything. You just need to create static environment for staging and production on Heruko through Heroku UI as it is necessary and add those names in the pipeline in staging and production part of the pipeline. 

## Usage
I have used shared runners provided as default by gitlab but you can choose to setup your own shared runner for this but for the sake of simplicity, use shared runners provided by default. 

## CI/CD Pipeline

Ci/CD pipelines is written to be completely automated. it has been written in a way like we write for big applications. Whenever a new developer needs to add a new feature, test it and ready to merge that feature into main branch, this pipeline will help to do so in an automatic way with a very little bit intervention of human. Checkout a new branch with a prefix "feature-" and make the changes to the code. 

Once the changes are made, commit it to the branch, pipelines will automatically create a test environment for test that feature with tests. Once the features are tested, maintainer will merge that branch into the main branch and code will be automatically deployed to the staging and production environment. 


## Support
I have used some environment variables which you can consult on the official gitlab documentation. 
https://docs.gitlab.com/ee/ci/variables/predefined_variables.html



## Authors and acknowledgment
Credits and thanks to my mentor who provided the templates and guidance in each step. 


## License
GNU 

## Project status
You can easily add new stages in this pipeline as you just need to create the stages and add to the pipeline but please choose the hierarchy well. 
